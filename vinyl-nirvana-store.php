<?php
/**
 * Plugin Name:     Vinyl Nirvana Store
 * Plugin URI:      http://vinylnirvana.com
 * Description:     Vinyl Nirvana Store actions, filters, etc.
 * Author:          Andre Gagnon
 * Author URI:      https://andregagnon.com
 * Text Domain:     vinyl-nirvana-store
 * Domain Path:     /languages
 * Version:         0.1.1
 *
 * @package         Vinyl_Nirvana_Store
 */

//////////////////////////////////////////////////////////////
//
// WooCommerce
//

// remove woocommerce nag message
add_filter( 'woocommerce_helper_suppress_admin_notices', '__return_true' );

// remove storefront theme credit
add_filter( 'storefront_credit_link', 'vns_filter_test');
function vns_filter_test( $value) {
  return( '');
}

// adjust storefront theme header
add_action( 'storefront_before_site', 'vns_header_theme_stuff', 0 );
function vns_header_theme_stuff() {
  // remove search
  remove_action( 'storefront_header', 'storefront_product_search', 40 );
  // move cart to where search was
  remove_action( 'storefront_header', 'storefront_header_cart', 60 );
  add_action( 'storefront_header', 'storefront_header_cart', 40 );
//    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
}

// add product search to sidebar
add_action( 'storefront_before_site', 'vns_sidebar_stuff', 10 );
function vns_sidebar_stuff() {

  //add_action( 'dynamic_sidebar_after', 'storefront_product_search', 40 );

}

// test
add_action( 'loop_start', 'vn_remove_theme_stuff', 0 );
function vn_remove_theme_stuff() {
//    remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
}

// test
add_action( 'storefront_sidebar', 'vn_test_action', 10);
function vn_test_action( $value='') {
  // if ( is_woocommerce() )
  //   echo '<b>Shopping</b>';
  global $post;
  //var_dump( $post);
}

// test
//add_action( 'storefront_page_before', 'vn_test_action_2', 10);
//add_action( 'storefront_page_after', 'vn_test_action_2', 10);
function vn_test_action_2( $value='') {
  echo '<b>test 2</b>';
}
//add_filter( 'storefront_related_products_args', 'vn_test_filter');
function vn_test_filter( $value) {
  return( '');
}


////////////////////////////////////////////////////////
// Miscellaneous

// add some custom CSS scripts
add_action( 'wp_enqueue_scripts', 'vns_scripts' );

function vns_scripts() {

  /**
   * Enqueue Dashicons style for frontend use when enqueuing your theme's style sheet
   */wp_enqueue_style( 'dashicons' );

  // custom script, load after storefront css
  wp_enqueue_style( 'vns-custom', plugins_url( 'vns-style.css', __FILE__), array('storefront-style'), false);
}
